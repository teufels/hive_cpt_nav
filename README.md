![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__cpt__nav-blue.svg)
![version](https://img.shields.io/badge/version-11.0.*-yellow.svg?style=flat-square)

HIVE > Navigation
==========
Mega & Mobile Navigation

#### This version supports TYPO3

![TYPO3Version](https://img.shields.io/badge/10_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_cpt_nav`

### How to use
- Install with composer
- Import Static Template (before hive_thm_custom)

### Notice
- developed with ExtensionBuilder

### Changelog
- 11.1.0 
  - include hive_cpt_nav_breadcrumb
  - inlcude JS/SCSS self (nogulp needed) // no Support for 10.4
- 11.0.* Version for Typo3 11.5 & 10.4
- 10.4.* Version for Typo3 10.4
- 9.5.* Version for Typo3 9.5