/***
 * @author Oguzhan Külcü - grafikcoder@gmail.com - oguzhankulcu.com
 * @version 1.1.0
 * http://oguzkulcu.github.io/smart-mobile-menu/
 */
(function ($) {
    "use strict";

    $.fn.smobileMenu = function (param) {
        // Start Code

        var thisContent = "__smobileMenuContent__menu";

        var durationOpen = parseInt($("#mobile-menu").data("duration-open"));
        var durationClose = parseInt($("#mobile-menu").data("duration-close"));
        var durationDropdown = parseInt(
            $("#mobile-menu").data("duration-dropdown")
        );

        var data = $.extend(
            {
                dropdown: true,
                getMenu: null,
                theme: "default",
                dropdownIcon: '<i class="drop-down arrow down"></i>',
                linkActiveClass: "active",
                activeAutoOpen: true,
                closeClickEvent: null, // function
                openClickEvent: null, // function
            },
            param
        );

        $("body").prepend('<nav id="' + thisContent + '"></nav>');

        var thisContentSelector = $("#" + thisContent),
            getMenuContent = $(data.getMenu).html(),
            theme_prefix = "smobilemenu-theme-";

        //var logoSVG = $(".logo-container .logo").append($("#__smobileMenuContent__menu").clone()).html();

        //thisContentSelector.html('<a href="/" id="smobileMenu-logo"><img src="fileadmin/user_upload/logos/zeag_topjob_logo_menu.svg" /></a><a href="#" id="smobileMenu-close-btn"></a><ul>' + getMenuContent + '</ul>');
        thisContentSelector.html(
            '<a href="#" id="smobileMenu-close-btn"></a><ul>' +
                getMenuContent +
                '</ul><div class="mobile-menu-divider"></div>'
        );

        //thisContentSelector.find('ul,li').removeAttr('class');

        var ul = thisContentSelector.children("ul");

        ul.addClass(theme_prefix + data.theme);
        ul.find("ul[data-smm-hidden=true],li[data-smm-hidden=true]").hide();
        ul.find("ul").hide();

        //    var countLi = ul.children('li').not('[data-smm-hidden=true]').length;

        if (data.dropdown) {
            ul.find("li a + ul")
                .not("ul[data-smm-dropdown=false]")
                .prev("a")
                .append(data.dropdownIcon);

            ul.find("li a + ul")
                .not("ul[data-smm-dropdown=false]")
                .prev("a")
                .on("click", function (e) {
                    $(this).next("ul").toggle(durationDropdown);
                    e.preventDefault();
                });
        }

        if (data.activeAutoOpen) {
            var active = ul.find("li a." + data.linkActiveClass);
            active.next("ul").show();
            active.closest("ul").show();
            active.closest("ul").parents("ul").show();
        }

        // close button
        $("#smobileMenu-close-btn").on("click", function (e) {
            if (data.closeClickEvent == null) {
                thisContentSelector.hide(durationClose);
            } else {
                data.closeClickEvent($(thisContentSelector));
            }
            e.preventDefault();
        });

        // data-smm-hidden

        $(this).on("click", function (e) {
            // openClickEvent

            if (data.openClickEvent == null) {
                thisContentSelector.show(durationOpen);
            } else {
                data.openClickEvent($(thisContentSelector));
            }
            e.preventDefault();
        });

        // End Code
    };
})(jQuery);
(function ($) {
    // version: 1.1.0
    $("[data-smart-menu]").each(function () {
        var selector = $(this).data("smart-menu");
        var theme = $(this).data("smart-menu-theme");
        var dropdown = $(this).data("smart-menu-dropdown");
        var active = $(this).data("smart-menu-active-auto");

        $(selector).smobileMenu({
            dropdown:
                dropdown !== null && dropdown !== undefined ? dropdown : true,
            getMenu: $(this),
            theme: theme !== null && theme !== undefined ? theme : "default",
            activeAutoOpen:
                active !== null && active !== undefined ? active : true,
        });
    });

    $("#open_mobile_menu").smobileMenu({
        dropdown: true,
        getMenu: "#mobile-menu",
    });
})(jQuery);
